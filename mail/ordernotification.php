



<?php 

function getDay($day)
{
    $tempDay = date('N', strtotime($day));
    if($tempDay == 1)
        return 'Senin';
    if($tempDay == 2)
        return 'Selasa';
    if($tempDay == 3)
       return 'Rabu';
    if($tempDay == 4)
        return 'Kamis';
    if($tempDay == 5)
        return 'Jumat';
    if($tempDay == 6)
        return 'Sabtu';
    if($tempDay == 7)
        return 'Minggu';
}


function getFullMonth($day)
{
    $tempMonth = date('n', strtotime($day));
    if($tempMonth == 1)
        return 'Januari';
    if($tempMonth == 2)
        return 'Februari';
    if($tempMonth == 3)
       return 'Maret';
    if($tempMonth == 4)
        return 'April';
    if($tempMonth == 5)
        return 'Mei';
    if($tempMonth == 6)
        return 'Juni';
    if($tempMonth == 7)
        return 'Juli';
    if($tempMonth == 8)
        return 'Agustus';
    if($tempMonth == 9)
        return 'September';
    if($tempMonth == 10)
        return 'Oktober';
    if($tempMonth == 11)
        return 'November';
    if($tempMonth == 12)
        return 'Desember';
}

function getFullDate($day)
{
    return getDay($day).' '.date('d', strtotime($day)).' '.getFullMonth($day).' '.date('Y', strtotime($day));
}

$datapengirim = '<div style="color:white;background-color:#5d5d5d;padding:7px 10px;font-weight:bold;text-align:left;">Data Pemesanan & Pengiriman</div>
<table style="border-collapse: collapse;width:100%;">
    <tr>
        <td style="width:200px;background-color:#e4e4e4;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">Pemesan</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">&nbsp;:&nbsp;</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">'.$model->customer->full_name.' ('.$model->customer->phone_no.')</td>
    </tr>
    <tr>
        <td style="width:200px;background-color:#e4e4e4;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">Email</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">&nbsp;:&nbsp;</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">'.$model->customer->email.'</td>
    </tr>';
if($model->customer->address == 1)
{
    $datapengirim .= '<tr>
        <td style="width:200px;background-color:#e4e4e4;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">Penerima</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">&nbsp;:&nbsp;</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">'.$model->customer->full_name.' ('.$model->customer->full_name.')</td>
    </tr>
    <tr>
        <td style="width:200px;background-color:#e4e4e4;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">Alamat Pengiriman</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">&nbsp;:&nbsp;</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">'.$model->customer->address.'</td>
    </tr>
    <tr>
        <td style="width:200px;background-color:#e4e4e4;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">Tanggal & Jam Pengiriman</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">&nbsp;:&nbsp;</td>
        <td style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;">'.getFullDate($model->created_date).' pk '.date('H:i',strtotime($model->jamPengiriman->jam_awal)).' - '.date('H:i',strtotime($model->jamPengiriman->jam_akhir)).'</td>
    </tr>';
}
$datapengirim .= '</table>';


$datapesanan .= '</tbody>

    <tfoot>
        <tr>
            <td colspan="4" class ="text-left" style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;text-align:left;">Order Total</td>
            <td colspan="4" class= "text-right" style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;text-align:right;">
                <label class = "label-total">'.number_format($model->total,0,',','.').'</label>
            </td>
        </tr>
        <tr>
            <td colspan="4" class ="text-right" style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;text-align:left;">Shipping Cost</td>
            <td class= "text-right" style="background-color:#e4e4e4;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;text-align:right;">
                <label class = "label-total">'.number_format($model->total_shipping_cost,0,',','.').'</label>
            </td>
        </tr>
        <tr>
            
            <th colspan="4" style="background-color:#5d5d5d;padding: 7px 10px;padding-right:0;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;color:white;text-align:left;">Grand Total</td>
            <th colspan="" style="background-color:#5d5d5d;padding: 7px 10px;border-collapse: collapse;border-bottom:1px solid #fff;border-top:1px solid #fff;color:white;text-align:right;">Rp '.number_format($model->total,0,',','.').'</td>
        </tr>
    </tfoot>
</table>';
?>

<html>
    <head>
        <style>
            div.scorebutton {
                margin-right:8px;
                border:1px solid gray;
                float:left;
                text-align:center;
                line-height:30px;
                width:30px;
                font-family:arial;
                font-size:14px;
                color:#222;
                margin-top:5px;
                margin-bottom: 5px;
            }

            div.scorebutton:hover {
                cursor: pointer;
                color:#fff;
                background-color:#a01019;
                border:1px solid #a01019;
            }

            a { text-decoration: none; }
        </style>
    </head>
    <body style="background-color:#ececec">
        <div style="podition:relative; margin-bottom:30px; margin-top:30px" align="center">
            <div style="position:relative; width:640px; background-color:white">
                <div style="position:relative;">
                    <div style="position:relative; background-color:#ececec; height:1px; width:520px; margin-top:30px; margin-bottom:30px"></div>
                    <div>
                        <img width="300px" src="order.bonami.co.id/web/images/bonami-bakery.png" align="center">
                    </div>
                    <br>
                    <div style="position:relative; font-size:14px; color:#2e2e2e; text-align:left; margin-bottom:20px">
                        <font face="Arial">Dear <b><?= $model->customer->full_name ?></b><br><br>
                        Berikut adalah data pesanan anda dengan nomor <b><?=$phone_no?></b>.</font>
                    </div>
                    <?= $datapesanan ?>
                    <br><br>
                    <?= $datapengirim ?>
                    <br><br>
                    <div style="position:relative; font-size:12px; color:#2e2e2e; text-align:left">
                        <font face="Arial">Silahkan melakukan pembayaran ke:<br/>
                            <b>BCA no 788.072.2811 a/n PT Bon Ami Abadi</b>
                        </font>
                    <div>
                    <br><br>
                    <div style="position:relative; font-size:12px; color:#2e2e2e; text-align:left">
                        <font face="Arial">Setelah melakukan pembayaran, mohon kirimkan bukti transfer melalui WhatsApp:<br/>
                            <a href="https://wa.me/6281131178798">https://wa.me/6281131178798</a>
                        </font>
                    <div>
                    <div style="position:relative; background-color:#ececec; height:1px; width:520px; margin-top:30px; margin-bottom:30px"></div>
                </div>
            </div>
        </div>
    </body>
</html>

