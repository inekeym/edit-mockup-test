<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/scrollbar.css',
        'css/bootstrap-icons/font/bootstrap-icons.css',
        'https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css',
        // 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback',
        // 'css/fontawesome-free/css/all.min.css',
        // 'css/icheck-bootstrap/icheck-bootstrap.min.css',
        // 'css/adminlte.min.css'
    ];
    public $js = [
        'https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
