<?php

namespace app\models;

use Yii;

class RiwayatPekerjaan extends \yii\db\ActiveRecord
{
    public function rules() {
      return [       
        [['id_pelamar','nama_perusahaan','posisi','pendapatan_terakhir','tahun_awal','tahun_akhir'],'required'],
       ];
     
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'riwayat_pekerjaan';
    }
  
    /**
     * {@inheritdoc}
     */
 
    /**
     * {@inheritdoc}
     */
   
    public function getPelamar()
    {
        return $this->hasOne(Pelamar::className(), ['id' => 'id_pelamar']);
    }
    
}
